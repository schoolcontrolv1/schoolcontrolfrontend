import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UsersComponent } from './components/admin/users/users.component';
import { CoursesComponent } from './components/admin/courses/courses.component';
import { TeacherComponent } from './components/admin/teacher/teacher.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StudentPublicComponent } from './components/student-public/student-public.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule} from '@angular/http';
import { SubjectsComponent } from './components/admin/subjects/subjects.component';
import { QualificationsComponent } from './components/admin/qualifications/qualifications.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './components/admin/sidebar/sidebar.component';
import { ActivateComponent } from './components/activate/activate.component';
import { PasswordResetInitComponent } from './components/password-reset-init/password-reset-init.component';
import { PasswordResetFinishComponent } from './components/password-reset-finish/password-reset-finish.component';
import { AdminProfileComponent } from './components/admin/admin-profile/admin-profile.component';
import { ModalModule } from 'ngx-bootstrap';
import { OptionalCourseComponent } from './components/admin/optional-course/optional-course.component';
import { CourseInformationComponent } from './components/admin/optional-course-tabs/course-information/course-information.component';
import { CourseTeacherComponent } from './components/admin/optional-course-tabs/course-teacher/course-teacher.component';
import { CourseStudentComponent } from './components/admin/optional-course-tabs/course-student/course-student.component';
import { CourseSubjectComponent } from './components/admin/optional-course-tabs/course-subject/course-subject.component';
import { Page404Component } from './components/page404/page404.component';
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    CoursesComponent,
    TeacherComponent,
    NavbarComponent,
    StudentPublicComponent,
    DashboardComponent,
    LoginComponent,
    SigninComponent,
    SignupComponent,
    SubjectsComponent,
    QualificationsComponent,
    SidebarComponent,
    ActivateComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    AdminProfileComponent,
    OptionalCourseComponent,
    CourseInformationComponent,
    CourseTeacherComponent,
    CourseStudentComponent,
    CourseSubjectComponent,
    Page404Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
