import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { WebService } from '../services/web.service';
import { AuthService } from '../services/auth.service';
import { SERVER } from '../config/server.config';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  public URL: any = '';
  public headers: any = '';

  constructor(
    private webService: WebService,
    private authService: AuthService,
    private _router: Router
  ) {
    this.URL = SERVER.ACCOUNT;
    this.headers = this.webService.JSONOptions(this.authService.getToken());
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.webService.get(this.URL, this.headers).pipe(map(
      (account) => {
        const can = account.authorities.includes('ROLE_USER');
        if (can) {
          return true;
        } else {
          return false;
        }
      }
    ));
  }

}
