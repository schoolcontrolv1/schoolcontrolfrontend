import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { WebService } from '../services/web.service';
import { SERVER } from '../config/server.config';
import { AuthService } from '../services/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(
    private webService: WebService,
    private authService: AuthService,
    private _router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.webService.get(SERVER.ACCOUNT, this.webService.JSONOptions(this.authService.getToken())).pipe(map(
      (account) => {
        const can = account.authorities.includes('ROLE_ADMIN');
        if (can) {
          return true;
        } else {
          if (!account.defaultUrl) {
            account.defaultUrl = 'dashboard';
          }
          this._router.navigate([account.defaultUrl]);
          return false;
        }
      }
    ));
  }
}
