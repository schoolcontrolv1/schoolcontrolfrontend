import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-password-reset-init',
  templateUrl: './password-reset-init.component.html',
  styleUrls: ['./password-reset-init.component.css']
})
export class PasswordResetInitComponent implements OnInit {

  emailForm: FormGroup;
  submitted = false;
  message: any = {};
  alert: any = false;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {
    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  ngOnInit() {
  }

  get formControls() {
    return this.emailForm.controls;
  }

  get formValue() {
    return this.emailForm.value;
  }

  sendResetPassword() {
    this.userService.passwordResetInit(this.formValue.email).subscribe(
      () => {
        this.message = this.showMessage('Email has been sent successfully', 'text-success', 6000);
        this.emailForm.clearValidators();
        this.emailForm.reset();
      },
      (err) => {
        this.message = this.showMessage(err.error.title, 'text-danger', 6000);
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    if (this.emailForm.invalid) {
      return;
    }
    this.sendResetPassword();
  }

  showMessage(message: any, alert: any, time: any) {
    const option = {
      message: message,
      alert: alert,
      time: time
    };
    setTimeout(() => {
      this.message = {};
    }, option.time);
    return option;
  }

}
