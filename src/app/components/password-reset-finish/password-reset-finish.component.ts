import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-password-reset-finish',
  templateUrl: './password-reset-finish.component.html',
  styleUrls: ['./password-reset-finish.component.css']
})
export class PasswordResetFinishComponent implements OnInit {


  key: String;
  password = '';
  passwordForm: FormGroup;
  submitted = false;
  message: any = '';
  alert: any = false;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      repeatPassword: ['', [Validators.required, Validators.minLength(8)]]
    }, {
        validator: this.validate.bind(this)
      }
    );
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.key = params['key'];
    });
  }

  validate(registrationFormGroup: FormGroup) {
    const password = registrationFormGroup.controls.password.value;
    const repeatPassword = registrationFormGroup.controls.repeatPassword.value;
    if (repeatPassword.length <= 0) {
      return null;
    }
    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }
    return null;
  }

  get formControls() {
    return this.passwordForm.controls;
  }

  get formValue() {
    return this.passwordForm.value;
  }

  passwordResetFinish() {
    const keyAndPassword = {
      'key': this.key,
      'newPassword': this.formValue.password
    };
    console.log(keyAndPassword);
    this.userService.passwordResetFinish(keyAndPassword).subscribe(
      (res) => {
        this.message = 'Success';
        this.alert = true;
        this.passwordForm.clearValidators();
        this.passwordForm.reset();
      },
      (error) => {
        this.message = error.error.title;
        this.alert = false;
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    if (this.passwordForm.invalid) {
      return;
    }
    this.passwordResetFinish();
  }

}
