import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentPublicComponent } from './student-public.component';

describe('StudentPublicComponent', () => {
  let component: StudentPublicComponent;
  let fixture: ComponentFixture<StudentPublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentPublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentPublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
