import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit {

  constructor(
    private _router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe(param => {
      let key = param.get('key');
      if (key !== null) {
        this.userService.activateAccount(key).subscribe(
          (res) => {
            this._router.navigate(['/login']);
          },
          (err) => {
            console.log('error');
          }
        )
      }
    });
  }

}
