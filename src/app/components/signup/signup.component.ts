import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  passwordFormGroup: FormGroup;
  message: any = {};
  data: any = {};
  submitted: any = false;
  authorityForm: FormGroup;
  typePassword: any = 'password';
  eye: any = 'mdi-eye-off';
  roleUser: any = 'ROLE_USER';
  roleDoctor: any = 'ROLE_DOCTOR';

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private _router: Router
  ) {
    this.signupForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      login: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      authorities: [[]]
    });
    this.authorityForm = new FormGroup({
      authority: new FormControl('ROLE_USER'),
    });
  }

  ngOnInit() {

  }

  get formControls() {
    return this.signupForm.controls;
  }

  get formValue() {
    return this.signupForm.value;
  }

  register() {
    this.data = Object.assign({}, this.formValue);
    this.data.authorities[0] = this.authorityForm.value.authority;
    this.userService.userRegister(this.data).subscribe(
      (res) => {
        this._router.navigate(['/login']);
      },
      (error) => {
        this.message = this.messages(error.error.title, 'alert-danger', true, 6000);
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    if (this.signupForm.invalid) {
      return;
    }
    this.register();
  }

  messages(message: any, alert: any, typeMessage: any, time: any) {
    const options: any = {
      message: message,
      alert: alert,
      typeMessage: typeMessage,
      time: time
    }
    setTimeout(() => {
      this.message = {};
    }, options.time);
    return options;
  }

  signIn() {
    this._router.navigate(['/login']);
  }

  eyePassword() {
    this.typePassword = this.typePassword === 'password' ? 'text' : 'password';
    this.eye = this.eye === 'mdi-eye-off' ? 'mdi-eye' : 'mdi-eye-off';
  }

}
