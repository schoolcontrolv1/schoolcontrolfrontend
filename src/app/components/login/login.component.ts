import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  typePassword: any = 'password';
  eye: any = 'mdi-eye-off';
  loginForm: FormGroup;
  account: any = {};
  token: any;
  getAccount: any;
  message: any = {};
  submitted: any = false;

  constructor(
    private authService: AuthService,
    private _router: Router,
    private formBuilder: FormBuilder,
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      rememberMe: [true]
    });
  }

  ngOnInit() {
  }

  get formControls() {
    return this.loginForm.controls;
  }

  get formValue() {
    return this.loginForm.value;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.login();
  }

  login() {
    this.account = Object.assign({}, this.formValue);
    this.authService.login(this.account).subscribe(
      (res) => {
        this.authService.logout();
        this.storage(res.id_token);
        this.getAccount = this.authService.getAccount().subscribe(
          (account) => {
            this.account = account;
            const can = account.authorities.includes('ROLE_ADMIN');
            if (can) {
              const url = ['/admin', {
                outlets: { sidebar: ['courses'] }
              }];
              this._router.navigate(url);
            } else {
              account.defacultUrl = '/dashboard';
              this._router.navigate([account.defacultUrl]);
            }
          },
          (error) => {
            this.message = this.messages(error.error.title, 'alert-danger', false, 8000);
          }
        );
      },
      (error) => {
        this.message = this.messages(error.error.title, 'alert-danger', false, 8000);
      }
    );
  }
  // hola -- nose  -- -- 111

  storage(token) {
    if (!this.loginForm.value.rememberMe) {
      localStorage.setItem('mfx-token', token);
    } else {
      sessionStorage.setItem('mfx-token', token);
    }
  }

  messages(message: any, alert: any, typeMessage: any, time: any) {
    const options: any = {
      message: message,
      alert: alert,
      typeMessage: typeMessage,
      time: time
    }
    setTimeout(() => {
      this.message = {};
    }, options.time);
    return options;
  }

  eyePassword() {
    this.typePassword = this.typePassword === 'password' ? 'text' : 'password';
    this.eye = this.eye === 'mdi-eye-off' ? 'mdi-eye' : 'mdi-eye-off';
  }

}
