import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { WebService } from 'src/app/services/web.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public account: any = {};

  constructor(
    private _router: Router,
    private userService: UserService,
    private webService: WebService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getAccount();
  }

  getAccount() {
    this.authService.getAccount().subscribe(
      (res) => {
        this.account = res;
      }
    );
  }

  hasAuthority(role: any) {
    if (Object.keys(this.account)) {
      if (this.account.authorities.includes(role)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  logout() {
    this.authService.logout();
    this._router.navigate(['/dashboard']);
  }

  //   <script type="text/javascript" >
  //   $(document).ready(function () {
  //     $('#sidebarCollapse').on('click', function () {
  //       $('#sidebar').toggleClass('active');
  //     });
  //   });
  // </script>

}
