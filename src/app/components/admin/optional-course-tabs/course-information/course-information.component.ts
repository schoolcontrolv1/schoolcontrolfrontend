import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseService } from 'src/app/services/course.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-course-information',
  templateUrl: './course-information.component.html',
  styleUrls: ['./course-information.component.css']
})
export class CourseInformationComponent implements OnInit {

  public courseId: any;
  public courseFormGroup: FormGroup;
  public submitted: any = false;

  constructor(
    private _router: Router,
    private route: ActivatedRoute,
    private courseService: CourseService,
    private formBuilder: FormBuilder
  ) { 
    this.route.parent.params.subscribe(
      (res) => {
        this.courseId = res.id;
      }
    );
    this.courseFormGroup = this.formBuilder.group({
      id: [null],
      title: ['', Validators.required],
      initials: ['', Validators.required],
      description: ['', Validators.required],
      activated: false
    });
  }

  ngOnInit() {
    this.getCourse();
  }

  onSubmit() {
    this.submitted = true;
    if(this.courseFormGroup.invalid) {
      return;
    }
    this.update();
  }

  get formControls() {
    return this.courseFormGroup.controls;
  }

  get formValue() {
    return this.courseFormGroup.value;
  }

  getCourse() {
    this.courseService.get(this.courseId).subscribe(
      (res) => {
        console.log(res);
        this.courseFormGroup.patchValue(res);
      },
      (err) => {
        const url = ['/admin', {
          outlets: { sidebar: ['optional-course', this.courseId, { outlets: {course: ['**']}}]}
        }];
        this._router.navigate(url);
      }
    );
  }

  update() {
    console.log(this.formValue);
    this.courseService.update(this.formValue).subscribe(
      (res) => {
        console.log('succes');
      },
      (error) => {
        console.log('error');
      }
    );
  }

}
