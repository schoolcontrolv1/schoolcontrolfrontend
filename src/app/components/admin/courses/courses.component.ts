import { Component, OnInit, TemplateRef } from '@angular/core';
import { CourseService } from 'src/app/services/course.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  public courseList: any = [];
  public modalRef: BsModalRef;
  public submitted: any = false; 
  public courseFormGroup: FormGroup;

  constructor(
    private courseService: CourseService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private _router: Router
  ) { 
    this.courseFormGroup = this.formBuilder.group({
      id: [null],
      title: ['', Validators.required],
      initials: ['', Validators.required],
      description: ['', Validators.required],
      activated: false
    });
  }

  ngOnInit() {
    this.getAll();
  }
  
  getAll() {
    console.log('save course');
    this.courseService.getAll().subscribe(
      (res) => {
        console.log(res);
        this.courseList = res;
      }
    )
  }

  get formControls() {
    return this.courseFormGroup.controls;
  }

  get formValue() {
    return this.courseFormGroup.value;
  }

  onSubmit() {
    this.submitted = true;
    if(this.courseFormGroup.invalid) {
      return;
    }
    this.create(this.formValue);
  }

  openModal(template: TemplateRef<any>, course?: any) {
    this.modalRef = this.modalService.show(template);
    if(course) {
      this.courseFormGroup.patchValue(course);
    }
  }

  create(course: any) {
    this.courseService.create(course).subscribe(
      (res) => {
        console.log(res);
        this.modalRef.hide();
        this.courseFormGroup.reset();
        this.getAll();
      },
      (err) => {
        console.log('error')
      }
    );
  }

  edit(course: any) {
    const url = ['/admin', {
      outlets: { sidebar: ['optional-course', course.id , { outlets: {course: ['course-information']}}]}
    }];
    this._router.navigate(url);
  }

  delete() {
    this.courseService.delete(this.formValue.id).subscribe(
      (res) => {
        console.log(res);
        this.modalRef.hide();
        this.getAll();
      },
      (err) => {
        console.log('error');
      }
    );
  }

}
