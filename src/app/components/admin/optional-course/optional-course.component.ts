import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-optional-course',
  templateUrl: './optional-course.component.html',
  styleUrls: ['./optional-course.component.css']
})
export class OptionalCourseComponent implements OnInit {

  public courseId: any;

  constructor(
    private _router: Router,
    private route: ActivatedRoute
  ) { 
    this.route.url.subscribe(
      (res) => {
        this.courseId = res[1].path; 
        console.log(res[1].path);
      }
    );
    this.navigate();
  }

  ngOnInit() {
  }

  navigate() {
    const url = ['/admin', {
      outlets: { sidebar: ['optional-course', this.courseId, { outlets: {course: ['course-information']}}]}
    }];
    this._router.navigate(url);
  }
}
