import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionalCourseComponent } from './optional-course.component';

describe('OptionalCourseComponent', () => {
  let component: OptionalCourseComponent;
  let fixture: ComponentFixture<OptionalCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionalCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
