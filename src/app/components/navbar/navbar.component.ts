import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/services/web.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public account: any = {};

  constructor(
    private authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getAccount();
  }

  getAccount() {
    if(this.authService.getToken()) {
      this.authService.getAccount().subscribe(
        (res) => {
          this.account = res;
        }
      );
    } else {
      this.account = null;
    }
  }

  logout() {
    this.authService.logout();
    this._router.navigate(['/dashboard']);
    this.getAccount();
  }

}
