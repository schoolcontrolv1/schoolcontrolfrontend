import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'schoolControlFrontend';

  constructor(
    private titleService: Title,
    private router: Router
  ) { }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title: string = (routeSnapshot.data && routeSnapshot.data['pageTitle']) ? routeSnapshot.data['pageTitle'] : 'schoolControl';
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  public hideNavbarOutlet() {
    return ((this.router.url !== '/login') &&
            (this.router.url !== '/signup') &&
            (this.router.url !== '/signin') &&
            (this.router.url !== '/sidebar') &&
            (this.router.url !== '/admin/courses') &&
            (this.router.url !== '/admin/subjects') &&
            (this.router.url !== '/admin/teachers') &&
            (this.router.url !== '/admin/quealifications') &&
            (this.router.url !== '/admin/history') &&
            (this.router.url !== '/admin/inscription') &&
            (this.router.url.includes('password-reset-init') !== true) &&
            (this.router.url.includes('password-reset-finish') !== true) &&
            (this.router.url.includes('/admin/(sidebar:courses)') !== true) &&
            (this.router.url.includes('/admin/(sidebar:teachers)') !== true) && 
            (this.router.url.includes('/admin/(sidebar:subjects)') !== true) &&
            (this.router.url.includes('/admin/(sidebar:qualifications)') !== true) &&
            (this.router.url.includes('/admin') !== true)
            );
  }

  public hideSidebarOutletAdmin() {
    return ((this.router.url !== '/navbar') &&
            (this.router.url !== '/login') &&
            (this.router.url !== '/singup') &&
            (this.router.url !== '/dashboard')); 
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.titleService.setTitle(this.getPageTitle(this.router.routerState.snapshot.root));
        }
    });
  }
}
