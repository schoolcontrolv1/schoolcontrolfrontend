import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { PasswordResetInitComponent } from './components/password-reset-init/password-reset-init.component';
import { PasswordResetFinishComponent } from './components/password-reset-finish/password-reset-finish.component';
import { ActivateComponent } from './components/activate/activate.component';
import { SidebarComponent } from './components/admin/sidebar/sidebar.component';
import { CoursesComponent } from './components/admin/courses/courses.component';
import { UsersComponent } from './components/admin/users/users.component';
import { TeacherComponent } from './components/admin/teacher/teacher.component';
import { QualificationsComponent } from './components/admin/qualifications/qualifications.component';
import { SubjectsComponent } from './components/admin/subjects/subjects.component';
import { AdminProfileComponent } from './components/admin/admin-profile/admin-profile.component';
import { AdminGuard } from './guards/admin.guard';
import { AuthGuard } from './guards/auth.guard';
import { OptionalCourseComponent } from './components/admin/optional-course/optional-course.component';
import { CourseTeacherComponent } from './components/admin/optional-course-tabs/course-teacher/course-teacher.component';
import { CourseInformationComponent } from './components/admin/optional-course-tabs/course-information/course-information.component';
import { CourseSubjectComponent } from './components/admin/optional-course-tabs/course-subject/course-subject.component';
import { CourseStudentComponent } from './components/admin/optional-course-tabs/course-student/course-student.component';
import { Page404Component } from './components/page404/page404.component';

const routes: Routes = [
  {
    path: '',
    component: NavbarComponent,
    outlet: 'navbar'
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'activate',
    component: ActivateComponent
  },
  {
    path: 'password-reset-init',
    component: PasswordResetInitComponent
  },
  {
    path: 'password-reset-finish',
    component: PasswordResetFinishComponent
  },
  {
    path: 'admin',
    component: SidebarComponent,
    children: [
      {
        path: '',
        redirectTo: '/admin/(sidebar:courses)',
        pathMatch: 'full'
      },
      {
        path: 'courses',
        component: CoursesComponent,
        outlet: 'sidebar'
      },
      {
        path: 'optional-course/:id',
        component: OptionalCourseComponent,
        outlet: 'sidebar',
        children: [
          // {
          //   path: '',
          //   redirectTo: '(sidebar:optional-course/:id/(course:course-information))',
          //   pathMatch: 'full'
          // },
          {
            path: 'course-information',
            component: CourseInformationComponent,
            outlet: 'course'
          },
          {
            path: 'course-subjects',
            component: CourseSubjectComponent,
            outlet: 'course'
          },
          {
            path: 'course-teachers',
            component: CourseTeacherComponent,
            outlet: 'course'
          },
          {
            path: 'course-students',
            component: CourseStudentComponent,
            outlet: 'course'
          },
          {
            path: '**',
            component: Page404Component,
            outlet: 'course'
          }
        ]
      },
      {
        path: 'users',
        component: UsersComponent,
        outlet: 'sidebar'
      },
      {
        path: 'teachers',
        component: TeacherComponent,
        outlet: 'sidebar'
      },
      {
        path: 'subjects',
        component: SubjectsComponent,
        outlet: 'sidebar'
      },
      {
        path: 'qualifications',
        component: QualificationsComponent,
        outlet: 'sidebar'
      },
      {
        path: 'admin-profile',
        component: AdminProfileComponent,
        outlet: 'sidebar'
      },
      {
        path: '**',
        component: Page404Component,
        outlet: 'sidebar'
      }
    ],
    canActivate: [AdminGuard, AuthGuard]
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }
