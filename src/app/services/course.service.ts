import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { WebService } from './web.service';
import { SERVER } from '../config/server.config';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(
    private authService: AuthService,
    private webService: WebService
  ) { }

  getAll() {
    const URL = SERVER.COURSES;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.get(URL, headers);
  }

  create(course: any) {
    const URL = SERVER.COURSES;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.post(URL, course, headers);
  }

  update(course: any) {
    const URL = SERVER.COURSES;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.put(URL, course, headers);
  }

  delete(id: any) {
    const URL = SERVER.COURSES + '/' + id;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.delete(URL, headers);
  }

  get(id: any) {
    const URL = SERVER.COURSES + '/' + id;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.get(URL, headers);
  }

}
