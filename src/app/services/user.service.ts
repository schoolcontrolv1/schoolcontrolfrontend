import { Injectable } from '@angular/core';
import { WebService } from './web.service';
import { SERVER } from '../config/server.config';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private webService: WebService,
    private authService: AuthService
  ) { }

  userRegister(user: any) {
    const URL = SERVER.REGISTER;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.post(URL, user, headers);
  }

  activateAccount(key) {
    return this.webService.get(SERVER.ACTIVATE + key);
  }
  
  passwordResetFinish(keyAndPassword) {
    const URL = SERVER.RESET_PASSWORD_FINISH;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.post(URL, keyAndPassword, headers);
  }


  passwordResetInit(mail) {
    const URL = SERVER.RESET_PASSWORD_INIT;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.post(URL, mail, headers);
  }

  getAll() {
    const URL = SERVER.USERS;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.get(URL, headers);
  }

  create(user: any) {
    const URL = SERVER.USERS;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.post(URL, user, headers);
  }

  update(user: any) {
    const URL = SERVER.USERS;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.put(URL, user, headers);
  }

  delete(login: any) {
    const URL = SERVER.USERS + "/" + login;
    const headers = this.webService.JSONOptions(this.authService.getToken());
    return this.webService.delete(URL, headers);
  }

}
